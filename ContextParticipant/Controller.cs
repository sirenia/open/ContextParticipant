﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Windows.Threading;
using Grpc.Core;
using Seacow;

namespace ContextParticipant
{
  class Controller
  {
    private readonly ContextManager.ContextManagerClient _cm;
    private readonly ContextData.ContextDataClient _cd;
    private readonly ContextSession.ContextSessionClient _cs;
    private readonly Model _m;
    private readonly Dispatcher _ui;
    private AsyncDuplexStreamingCall<ContextParticipantStreamRequest, ContextParticipantStreamResponse> _stream;
    private Registry.RegistryClient _cr;
    private ContextAction.ContextActionClient _ca;

    public Controller(Model model, Dispatcher ui, string connection)
    {
      var conn = new Uri(connection);
      var c = new Channel(conn.Host, conn.Port, ChannelCredentials.Insecure);
      _cm = new ContextManager.ContextManagerClient(c);
      _cs = new ContextSession.ContextSessionClient(c);
      _ca = new ContextAction.ContextActionClient(c);
      _cd = new ContextData.ContextDataClient(c);
      _cr = new Registry.RegistryClient(c);
      _m = model;
      _ui = ui;
    }

    public string CreateSession(string tag)
    {
      try
      {
        var cr = new CreateRequest {ComponentIdentifier = "0", Tag = tag};
        var response = _cs.Create(cr);
        return response.ManagerID;
      }
      catch (Exception e)
      {
        _m.Log(LogSeverity.Error, e.Message);
      }

      return null;
    }

    public void JoinStreaming(string session)
    {
      if (string.IsNullOrEmpty(session))
      {
        _m.Log(LogSeverity.Info, "No session defined");
      }

      if (!string.IsNullOrEmpty(_m.Session))
      {
        Disconnect();
      }

      _m.Session = session;

      var joinRequest = new JoinCommonContextRequest
      {
        ApplicationName = "bgrm138p43kg008eem3g",
        ComponentIdentifier = session,
        Participant = "grpc://doesntmatter",
        Survey = true,
        Wait = true
      };

      _stream = _cm.ContextParticipantStream();
     
      // We start by spinning of the handling of incoming "responses" to a seperate thread
      Task.Run(() => HandleStream(_stream));

      // Join the common context, this must be done in-stream
      var t = _stream.RequestStream.WriteAsync(new ContextParticipantStreamRequest { JoinCommonContext = joinRequest });
      t.ContinueWith(task => _m.Log(LogSeverity.Info, "Join - "+task.Status));

      // ... the stream handling should then handle the rest of the communication
    }

    public void Disconnect()
    {
      try
      {
        var leaveRequest = new LeaveCommonContextRequest
        {
          ComponentIdentifier = _m.Session,
          ParticipantCoupon = _m.ParticipantCoupon
        };
        _cm.LeaveCommonContext(leaveRequest);
        // Avoid leaking streams
        _stream.Dispose();
        _stream = null;
        _m.Log(LogSeverity.Info, $"Left {_m.Session}");
        // Model cleanup
        _m.Status = "Disconnected";
        _m.ParticipantCoupon = -1;
        _m.Context.Clear();
      }
      catch (Exception e)
      {
        _m.Log(LogSeverity.Error, e.Message);
      }
    }

    private async Task HandleStream(AsyncDuplexStreamingCall<ContextParticipantStreamRequest, ContextParticipantStreamResponse> stream)
    {
      // *A note on terminology* the terms request and response are switched in terms of their semantics here since
      // it is normally the client which sends requests to the server and and gets a response, but in this case it is
      // the server which (after the initial JoinCommonContext) initiates communication by sending "responses".

      try
      {
        // This protocol is basically reactive from the clients perspective; we wait for a "response" and then send the appropriate "request"
        while (await stream.ResponseStream.MoveNext())
        {
          var response = stream.ResponseStream.Current;
          switch (response.ContentCase)
          {
            case ContextParticipantStreamResponse.ContentOneofCase.JoinCommonContext:
              _m.Log(LogSeverity.Info, $"Got participant coupon - {response.JoinCommonContext.ParticipantCoupon} from {response.JoinCommonContext.ComponentID}");
              _m.ParticipantCoupon = response.JoinCommonContext.ParticipantCoupon;
              _m.Session = response.JoinCommonContext.ComponentID;

              // Refresh the state
              var mostRecentContextCouponResponse = _cm.MostRecentContextCoupon(new MostRecentContextCouponRequest
                {ComponentIdentifier = _m.Session});
              RetrieveState(mostRecentContextCouponResponse.ContextCoupon);

              // Get some information about the CM
              var implInfoRequest = new ImplementationInformationRequest {ComponentIdentifier = _m.Session};
              var implInfoResponse = _cm.ImplementationInformation(implInfoRequest);
              _m.Version =
                $"{implInfoResponse.RevMajorNum}.{implInfoResponse.RevMinorNum}.{implInfoResponse.RevMaintenanceNum}";
              _m.Manufacturer = implInfoResponse.Manufacturer;

              _m.Status = $"Joined context {_m.Session} (v{_m.Version} by {_m.Manufacturer})";
              break;
            case ContextParticipantStreamResponse.ContentOneofCase.Ping:
              // We need to stream back a Pong in order to stay in the context
              await stream.RequestStream.WriteAsync(new ContextParticipantStreamRequest
              {
                Pong = new ContextParticipantStreamRequest.Types.PongRequest()
              });
              break;
            case ContextParticipantStreamResponse.ContentOneofCase.ContextChangesAccepted:
              // TODO 1) Get state from CM, then 2) update internal state to match
              _m.Log(LogSeverity.Info,
                $"ContextChangesAccepted for context coupon - {response.ContextChangesAccepted.ContextCoupon}");
              // Refresh the state
              RetrieveState(response.ContextChangesAccepted.ContextCoupon);
              break;
            case ContextParticipantStreamResponse.ContentOneofCase.ContextChangesCanceled:
              // TODO React soberly to a canceled tx
              _m.Log(LogSeverity.Warn,
                $"ContextChangesCanceled for context coupon - {response.ContextChangesCanceled.ContextCoupon}");
              break;
            case ContextParticipantStreamResponse.ContentOneofCase.ContextChangesPending:
              // TODO Can we accept the incoming changes?
              _m.Log(LogSeverity.Info,
                $"ContextChangesPending for context coupon - {response.ContextChangesPending.ContextCoupon}");
              // HACK Yes, we can
              var yes = new ContextParticipantStreamRequest.Types.ContextChangesPendingRequest
              {
                ContextCoupon = response.ContextChangesPending.ContextCoupon,
                Decision = "accept",
                Reason = "I will always comply"
              };
              await stream.RequestStream.WriteAsync(new ContextParticipantStreamRequest {ContextChangesPending = yes});
              break;
            case ContextParticipantStreamResponse.ContentOneofCase.ContextChangesTerminated:
              // TODO React appropriately
              _m.Log(LogSeverity.Warn, $"ContextChangesTerminated");
              break;
            case ContextParticipantStreamResponse.ContentOneofCase.Error:
              // We'll get an error back if we did not follow the protocol
              _m.Log(LogSeverity.Error, $"Got error from CM: {response.Error.Error}");
              break;
            default:
              _m.Log(LogSeverity.Warn, $"Got {response.ContentCase} - did not expect that");
              break;
          }
        }
      }
      catch (Exception e)
      {
        _m.Log(LogSeverity.Warn, e.Message);
      }

      _m.Log(LogSeverity.Info, "Disconnected");
    }

    public void RetrieveState(long contextCoupon)
    {
      _m.Log(LogSeverity.Info, "Updating from CM");
      // Get names of complete state
      var getItemNamesResponse = _cd.GetItemNames(new GetItemNamesRequest {ComponentIdentifier = _m.Session, ContextCoupon = contextCoupon});
      _m.Log(LogSeverity.Info, $"Got names = {getItemNamesResponse.Names}");
      var getItemValuesResponse = _cd.GetItemValues(new GetItemValuesRequest
      {
        ComponentIdentifier = _m.Session,
        ContextCoupon = contextCoupon,
        ItemNames = {getItemNamesResponse.Names},
        OnlyChanges = false
      });

      _m.Log(LogSeverity.Info, $"Got values = {getItemValuesResponse.ItemValues}");

      _ui.Invoke(() =>
      {
        _m.Context.Clear();
        for (var i = 0; i < getItemValuesResponse.ItemValues.Count; i++)
        {
          _m.Context.Add(new ContextItem { Subject = getItemValuesResponse.ItemValues[i], Value = getItemValuesResponse.ItemValues[++i] });
        }
      });
    }

    public long RunTx(IList<ContextItem> changes)
    {
      try
      {
        // Start
        var startContextChangesResponse = _cm.StartContextChanges(new StartContextChangesRequest
        {
          ComponentIdentifier = _m.Session,
          ParticipantCoupon = _m.ParticipantCoupon
        });
        var cc = startContextChangesResponse.ContextCoupon;

        // SetItemValues
        var setItemValues = new SetItemValuesRequest
        {
          ComponentIdentifier = _m.Session,
          ParticipantCoupon = _m.ParticipantCoupon,
          ContextCoupon = cc
        };
        setItemValues.ItemNames.AddRange(changes.Select(ci => ci.Subject));
        setItemValues.ItemValues.AddRange(changes.Select(ci => ci.Value));
        _cd.SetItemValues(setItemValues);

        // End
        var endContextChangesResponse =
          _cm.EndContextChanges(new EndContextChangesRequest {ComponentIdentifier = _m.Session, ContextCoupon = cc});
        _m.Log(LogSeverity.Info,
          $"I got EndContextChanges response: noContinue={endContextChangesResponse.NoContinue}");

        // Publish result
        _cm.PublishChangesDecision(new PublishChangesDecisionRequest
        {
          ComponentIdentifier = _m.Session,
          ContextCoupon = cc,
          Decision = endContextChangesResponse.NoContinue ? "cancel" : "accept"
        });

        return cc;
      }
      catch (Exception e)
      {
        _m.Log(LogSeverity.Error, e.Message);
      }

      return -1;
    }

    public void Commit()
    {
      RunTx(_m.Context);
    }

    public void Delete(ContextItem contextItem)
    {
      try
      {
        // Start
        var startContextChangesResponse = _cm.StartContextChanges(new StartContextChangesRequest
        {
          ComponentIdentifier = _m.Session,
          ParticipantCoupon = _m.ParticipantCoupon
        });
        var cc = startContextChangesResponse.ContextCoupon;

        // DeleteItems
        var deleteItems = new DeleteItemsRequest
        {
          ComponentIdentifier = _m.Session,
          ContextCoupon = cc,
          ParticipantCoupon = _m.ParticipantCoupon,
          ItemNames = {contextItem.Subject}
        };
        _cd.DeleteItems(deleteItems);

        // End
        var endContextChangesResponse =
          _cm.EndContextChanges(new EndContextChangesRequest {ComponentIdentifier = _m.Session, ContextCoupon = cc});
        _m.Log(LogSeverity.Info,
          $"I got EndContextChanges response: noContinue={endContextChangesResponse.NoContinue}");

        // Publish result
        _cm.PublishChangesDecision(new PublishChangesDecisionRequest
        {
          ComponentIdentifier = _m.Session,
          ContextCoupon = cc,
          Decision = endContextChangesResponse.NoContinue ? "cancel" : "accept"
        });
      }
      catch (Exception e)
      {
        _m.Log(LogSeverity.Error, e.Message);
      }
    }

    public void ListSessions()
    {
      var response = _cr.OfType(new OfTypeRequest {TypeName = "ContextManager"});
      foreach (var id in response.Identifiers)
      {
        _m.Log(LogSeverity.Info, $"Session: {id}");
      }
    }
    
    public void ListActions()
    {
      var response = _cr.LocalActions(new LocalActionsRequest());
      foreach (var a in response.Actions)
      {
        _m.Log(LogSeverity.Info, $"Action: Id={a.Identifier}, Name={a.Name}, App={a.ApplicationDescription},{a.ApplicationName}");
      }
    }

    public void RunAction(string action)
    {
      try
      {
        _ca.Perform(new PerformRequest
          {ActionIdentifier = action, ParticipantCoupon = _m.ParticipantCoupon, ComponentIdentifier = _m.Session});
      }
      catch (Exception e)
      {
        _m.Log(LogSeverity.Warn, e.Message);
      }
    }
  }
}
