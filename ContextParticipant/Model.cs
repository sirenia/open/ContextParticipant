﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ContextParticipant.Annotations;

namespace ContextParticipant
{
  class Model : INotifyPropertyChanged
  {
    private string _status;
    private long _participantCoupon = -1;
    private string _session;
    private string _logContent;
    private ObservableCollection<ContextItem> _context;
    private string _manufacturer;
    private string _version;

    public string Status
    {
      get { return _status; }
      set { _status = value; OnPropertyChanged(nameof(Status)); }
    }

    public long ParticipantCoupon
    {
      get { return _participantCoupon; }
      set { _participantCoupon = value; OnPropertyChanged(nameof(ParticipantCoupon)); }
    }

    public string Session
    {
      get { return _session; }
      set { _session = value; OnPropertyChanged(nameof(Session)); }
    }

    public ObservableCollection<ContextItem> Context
    {
      get { return _context; }
      set
      {
        _context = value;
        OnPropertyChanged(nameof(Context));
      }
    }

    public string LogContent
    {
      get { return _logContent; }
      set { _logContent = value; OnPropertyChanged(nameof(LogContent)); }
    }

    public string Version
    {
      get { return _version; }
      set { _version = value; OnPropertyChanged(nameof(Version)); }
    }

    public string Manufacturer
    {
      get { return _manufacturer; }
      set { _manufacturer = value; OnPropertyChanged(nameof(Manufacturer)); }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public void Log(LogSeverity l, string s)
    {
      _logContent += $"«{l}» {s}{Environment.NewLine}"; 
      OnPropertyChanged(nameof(LogContent));
    }

    public Model()
    {
      Context = new ObservableCollection<ContextItem>();
    }
  }

  internal class ContextItem : INotifyPropertyChanged
  {
    private string _value;
    private string _subject;


    public string Subject
    {
      get { return _subject; }
      set
      {
        _subject = value;
        OnPropertyChanged(nameof(Subject));
      }
    }

    public string Value
    {
      get { return _value; }
      set
      {
        _value = value;
        OnPropertyChanged(nameof(Value));
      }
    }

    #region Equality members

    protected bool Equals(ContextItem other)
    {
      return string.Equals(Subject, other.Subject);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != this.GetType()) return false;
      return Equals((ContextItem) obj);
    }

    public override int GetHashCode()
    {
      return (Subject != null ? Subject.GetHashCode() : 0);
    }

    #endregion

    [Browsable(false)]
    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }

  internal enum LogSeverity
  {
    Info,
    Warn,
    Error,
  }
}
