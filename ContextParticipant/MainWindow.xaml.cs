﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Grpc.Core;
using Seacow;

namespace ContextParticipant
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private Controller _controller;

    public MainWindow()
    {
      InitializeComponent();

      
      //TextBoxTag.Text = Environment.MachineName;

      // Setup model
      DataContext =  new Model();;
    }

    private void BtnCommit_Click(object sender, RoutedEventArgs e)
    {
      _controller.Commit();
    }

    private void BtnCreate_Click(object sender, RoutedEventArgs e)
    {

      try
      {
        _controller = new Controller(DataContext as Model, Dispatcher.CurrentDispatcher, TbUrl.Text);
        //var sessionId = _controller.CreateSession(TextBoxTag.Text);
        _controller.ListSessions();
        _controller.ListActions();
        _controller.JoinStreaming(string.Empty);
      }
      catch (Exception err)
      {
        Console.Error.WriteLine(err);
      }
    }

    private void BtnDisconnect_Click(object sender, RoutedEventArgs e)
    {
      _controller.Disconnect();
    }

    private void BtnDeleteSelected_Click(object sender, RoutedEventArgs e)
    {
      _controller.Delete(ContextDataGrid.SelectedValue as ContextItem);
    }

    private void BtnRun_Click(object sender, RoutedEventArgs e)
    {
      _controller.RunAction(TbAction.Text);
    }
  }
}
